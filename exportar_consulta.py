

import time
import pandas as pd
from services.consulta_arbol_nosql import consultar_arbol_familiar_nosql


if __name__ == '__main__':
    identificacion = '8_SAL_FI004_18_ARBOL'

    datos_cedula = [
	'xxxxxxxxxxxxx'
    ]

    informacion_total = []

    for row in range(len(datos_cedula)):
        try:
            informacion_total.append(
                consultar_arbol_familiar_nosql(datos_cedula[row]))
        except Exception as e:
            print(e)
            with open('cedulasErrorNew.txt', 'a') as f:
                f.write(f" {datos_cedula[row]} --> Error: {e}\n")

    df_informacion_total = pd.DataFrame()
    if (informacion_total):
        for info in informacion_total:
            if (info is not None):
                for inf in info:
                    if (inf):
                        row_insert = {"cedula": inf['cedula'],
                                      "nombresUsuario": inf['nombresUsuario'], "fechaNacimiento": inf['fechaNacimiento'],
                                      "estadoCivil": inf['estadoCivil'], "cedulaPadre": inf['cedulaPadre'],
                                      "nombresPadre": inf['nombresPadre'], "cedulaMadre": inf['cedulaMadre'],
                                      "nombresMadre": inf['nombresMadre'], "cedulaConyugue": inf['cedulaConyuge'],
                                      "nombresConyugue": inf['nombreConyuge'], "nivel": inf['nivel'],
                                      "tipo": inf['tipo'], "identificacionUsuarioConsultado": inf['identificacionUsuarioConsultado'],
                                      "nombresUsuarioConsultado": inf['nombresUsuarioConsultado']}
                        df_new_row = pd.DataFrame(row_insert, index=[0])
                        df_informacion_total = pd.concat(
                            [df_informacion_total, df_new_row], ignore_index=True)
                        df_informacion_total = df_informacion_total.drop_duplicates(
                            ignore_index=True)

    nombre_archivo = identificacion + "_" + time.strftime("%Y-%m-%d") + ".xlsx"
    df_informacion_total.to_excel(nombre_archivo)
    print("##############################################################################################")
    print("\n")
    print("##################################### Proceso Terminado ######################################")
    print("\n")
    print("##############################################################################################")
    print("\n")
    print(f"Se ha exportado el archivo {nombre_archivo}")
