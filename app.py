from flask import Flask
from api.auth import init as init_auth_routes
from api.routes import init as init_routes
from api.consumo_servicio_web import init as init_conn_regs
from flask_wtf.csrf import CSRFProtect
from flask_cors import CORS

def create_app():
    app = Flask(__name__)
    CORS(app, support_credentials=True)
    init_auth_routes(app)
    init_routes(app)
    init_conn_regs(app)
    return app

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', debug=True, port=5005)