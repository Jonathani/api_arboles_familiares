# Arboles Familiares
Este proyecto tiene como objetivo generar los arboles familiares de una persona, dada la cédula de identidad, con un 
cuarto grado de consanguinidad y segundo grado de afinidad.

## Requisitos

- python3.9 o superior
- pip 22 o superior
- Base de datos Postgres
- Base de datos MongoDB
- docker
- docker-compose.yml
- DockerFile

## Ejecución como proceso

- Instalar las librerias que se necesitan para ejecutar en el programa. Las librerias que se necesitan estan en el archivo requirements.txt. Se debe instalar mediante el comando pip install -r requerements.txt
- Para ejecutar el programa se debe escribir el siguiente comando python main.py
- Cambiar la información de conexión a la bdd stage en el archivo conexion.py

## Ejecución como api

- Levantar api
    - Construir la api/docker con el comando: docker-compose build
    - Desplegar la api/docker con el comando: docker-compose up (-d si se desea como demonio)

- Uso:
    - Realizar primero a la petición: http://[ip_host]:5005/authenticate, enviando en el body como json: {"user": "admin", "password": "password"}. Con esta petición devolverá un token, el cual se usará para enviar en la siguiente api
    - Consumir la api realizando la petición: http://localhost:5005/api/consulta/arbolesFamiliaresNOSQL (Consumo a Mongo) y http://localhost:5005/api/consulta/arbolesFamiliaresSQL (Consumo a Postgres), enviando en el header como parámetro: Authorization: Token y en el body como json: { "cedulasConsultar": "[nro_cedula]" }
