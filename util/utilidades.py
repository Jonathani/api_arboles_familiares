from datetime import date
from entities.conexion import cursor_stage
import pandas as pd
from entities.entitySQL.Cedulas import TeDigercicCedulado, session
from entities.Persona import Persona


class Utilidades:

    @staticmethod
    def limpiar_espacios(dato):
        dato_limpio = ""
        dato_sin_espacio = dato.split(" ")
        for row in range(len(dato_sin_espacio)):
            if (dato_sin_espacio[row] != ""):
                dato_limpio = dato_limpio + dato_sin_espacio[row] + " "

        return dato_limpio.strip()

    @staticmethod
    def remove_duplicates(datos):

        seen = set()
        new_list = []
        for d in datos:
            t = tuple(d.items())
            if t not in seen:
                seen.add(t)
                new_list.append(d)

        return new_list

    @staticmethod
    def calculo_edad(birth_date):
        today = date.today()
        age = today.year - birth_date.year - \
            ((today.month, today.day) < (birth_date.month, birth_date.day))
        return age

    @staticmethod
    def validar_cedula(texto):
        # sin ceros a la izquierda
        if (texto is not None and int(texto) != 0):
            texto = Utilidades.completar_cedula(str(texto))
        else:
            return False

        nocero = texto.strip("0")

        cedula = int(nocero, 0)
        verificador = cedula % 10
        numero = cedula//10

        # mientras tenga números
        suma = 0
        while (numero > 0):

            # posición impar
            posimpar = numero % 10
            numero = numero//10
            posimpar = 2*posimpar
            if (posimpar > 9):
                posimpar = posimpar-9

            # posición par
            pospar = numero % 10
            numero = numero//10

            suma = suma + posimpar + pospar

        decenasup = suma//10 + 1
        calculado = decenasup*10 - suma
        if (calculado >= 10):
            calculado = calculado - 10

        if (calculado == verificador):
            validado = True
        else:
            validado = False

        return (validado)

    @staticmethod
    def completar_cedula(cedula):
        if (len(str(cedula)) == 9):
            cedula = '0' + str(cedula)
        else:
            cedula = str(cedula)

        return cedula

    @staticmethod
    def definir_tipo(tipo, sexo):
        if (tipo == 'Padres'):
            tipo = 'Padre' if sexo == 1 else 'Madre'
        elif (tipo == 'Hijos'):
            tipo = 'Hijo' if sexo == 1 else 'Hija'
        elif (tipo == 'Hermanos'):
            tipo = 'Hermano' if sexo == 1 else 'Hermana'
        elif (tipo == 'HermanosMadre'):
            tipo = 'HermanoMadre' if sexo == 1 else 'HermanaMadre'
        elif (tipo == 'HermanosPadre'):
            tipo = 'HermanoPadre' if sexo == 1 else 'HermanaPadre'
        elif (tipo == 'AbuelosPadre'):
            tipo = 'AbueloPadre' if sexo == 1 else 'AbuelaPadre'
        elif (tipo == 'AbuelosMadre'):
            tipo = 'AbueloMadre' if sexo == 1 else 'AbuelaMadre'
        elif (tipo == 'Nietos'):
            tipo = 'Nieto' if sexo == 1 else 'Nieta'
        elif (tipo == 'BisabuelosPadre'):
            tipo = 'BisabueloPadre' if sexo == 1 else 'BisabuelaPadre'
        elif (tipo == 'BisabuelosMadre'):
            tipo = 'BisabueloMadre' if sexo == 1 else 'BisabuelaMadre'
        elif (tipo == 'Bisnietos'):
            tipo = 'Bisnieto' if sexo == 1 else 'Bisnieta'
        elif (tipo == 'Tios'):
            tipo = 'Tio' if sexo == 1 else 'Tia'
        elif (tipo == 'Sobrinos'):
            tipo = 'Sobrino' if sexo == 1 else 'Sobrina'
        elif (tipo == 'Tatarabuelos'):
            tipo = 'Tatarabuelo' if sexo == 1 else 'Tatarabuela'
        elif (tipo == 'Tataranietos'):
            tipo = 'Tataranieto' if sexo == 1 else 'Tataranieta'
        elif (tipo == 'Primos'):
            tipo = 'Primo' if sexo == 1 else 'Prima'
        elif (tipo == 'TiosAbuelos'):
            tipo = 'TioAbuelo' if sexo == 1 else 'TiaAbuela'
        elif (tipo == 'SobrinosNietos'):
            tipo = 'SobrinoNieto' if sexo == 1 else 'SobrinaNieta'
        elif (tipo == 'Suegros'):
            tipo = 'Suegro' if sexo == 1 else 'Suegra'
        elif (tipo == 'Yernos'):
            tipo = 'Yerno' if sexo == 1 else 'Nuera'
        elif (tipo == 'Padrastros'):
            tipo = 'Padrastro' if sexo == 1 else 'Madrastra'
        elif (tipo == 'Hijastros'):
            tipo = 'Hijastro' if sexo == 1 else 'Hijastra'
        elif (tipo == 'AbuelosConyugue'):
            tipo = 'Abuelo Conyugue' if sexo == 1 else 'Abuela Conyugue'
        elif (tipo == 'Cuniados'):
            tipo = 'Cuñado' if sexo == 1 else 'Cuñada'
        elif (tipo == 'Hermanastros'):
            tipo = 'Hermanastro' if sexo == 1 else 'Hermanastra'
        elif (tipo == 'Abuelastros'):
            tipo = 'Abuelastro' if sexo == 1 else 'Abuelastra'
        elif (tipo == 'Nietrastos'):
            tipo = 'Nietrasto' if sexo == 1 else 'Nietrasta'
        elif (tipo == 'Abuelos'):
            tipo = 'Abuelo' if sexo == 1 else 'Abuela'
        elif (tipo == 'Bisabuelos'):
            tipo = 'Bisabuelo' if sexo == 1 else 'Bisabuela'
        return tipo

    @staticmethod
    def armar_informacion(row_stage, nivel, tipo, identificacion_usuario, nombres_usuario):
        if (row_stage[1]):
            tipo = Utilidades.definir_tipo(tipo, row_stage[1])

        dato = {"cedula": Utilidades.completar_cedula(row_stage[0]), "nombresUsuario": row_stage[2],
                "fechaNacimiento": row_stage[3], "estadoCivil": row_stage[4], "cedulaPadre": Utilidades.completar_cedula(row_stage[5]),
                "nombresPadre": row_stage[6], "cedulaMadre": Utilidades.completar_cedula(row_stage[7]),  "nombresMadre": row_stage[8],
                "cedulaConyuge": Utilidades.completar_cedula(row_stage[9]), "nombreConyuge": row_stage[10], "nivel": nivel, "tipo": tipo,
                "identificacionUsuarioConsultado": identificacion_usuario, "nombresUsuarioConsultado": nombres_usuario}
        return dato

    @staticmethod
    def armar_informacion_objeto(row_stage, nivel, tipo, identificacion_usuario, nombres_usuario):
        if (row_stage.cod_sexo):
            tipo = Utilidades.definir_tipo(tipo, row_stage.cod_sexo)

        fecha_nacimiento = row_stage.fecha_nacimiento.strftime("%Y-%d-%m") if row_stage.fecha_nacimiento else row_stage.fecha_nacimiento
        dato = {"cedula": Utilidades.completar_cedula(row_stage.cedula),
                "nombresUsuario": row_stage.nombres.replace("'", "") if row_stage.nombres else None,
                "fechaNacimiento": fecha_nacimiento,
                "estadoCivil": row_stage.des_estado_civil.replace("'", "") if row_stage.des_estado_civil else None,
                "cedulaPadre": Utilidades.completar_cedula(row_stage.cedula_padre),
                "nombresPadre": row_stage.nombre_padre.replace("'", "") if row_stage.nombre_padre else None,
                "cedulaMadre": Utilidades.completar_cedula(row_stage.cedula_madre),
                "nombresMadre": row_stage.nombre_madre.replace("'", "") if row_stage.nombre_madre else None,
                "cedulaConyuge": Utilidades.completar_cedula(row_stage.cedula_conyuge),
                "nombreConyuge": row_stage.nombre_conyuge.replace("'", "") if row_stage.nombre_conyuge else None, 
                "nivel": nivel, 
                "tipo": tipo,
                "identificacionUsuarioConsultado": identificacion_usuario, 
                "nombresUsuarioConsultado": nombres_usuario}
        return dato

    @staticmethod
    def armar_informacion_sin_datos(nombres, nivel, tipo, identificacion_usuario, nombres_usuario):

        dato = {"cedula": None, "nombresUsuario": nombres,
                "fechaNacimiento": None, "estadoCivil": None, "cedulaPadre": None,
                "nombresPadre": None, "cedulaMadre": None,  "nombresMadre": None,
                "cedulaConyuge": None, "nombreConyuge": None, "nivel": nivel, "tipo": tipo,
                "identificacionUsuarioConsultado": identificacion_usuario, "nombresUsuarioConsultado": nombres_usuario}
        return dato

    @staticmethod
    def armar_array_sin_datos(nombres):
        dato = (None, None, nombres, None, None,
                None, None, None, None, None, None)

        return dato

    @staticmethod
    def armar_objeto(nombres, sexo):
        persona = Persona()
        persona.cedula = 0
        persona.cod_sexo = sexo   
        persona.nombres = nombres
        persona.fecha_nacimiento = ""
        persona.des_estado_civil = ""
        persona.cedula_padre = 0
        persona.nombre_padre = ""
        persona.cedula_madre = 0
        persona.nombre_madre = ""
        persona.cedula_conyuge = 0
        persona.nombre_conyuge = ""

        return persona

    @staticmethod
    def lista_sin_repetidos_objetos(lista_en_comun, lista_total):

        datos = []
        lista_en_comun_aux = []
        lista_total_aux = []        

        if (lista_total and lista_en_comun):

            for s in lista_en_comun:
                if (s is not None):
                    lista_en_comun_aux.append(s.to_dict())

            for s in lista_total:
                if (s is not None):
                    lista_total_aux.append(s.to_dict())

            df_lista_en_comun = pd.DataFrame.from_records(lista_en_comun_aux)
            df_lista_en_comun = df_lista_en_comun.drop_duplicates(ignore_index=True)

            df_lista_total = pd.DataFrame.from_records(lista_total_aux)
            df_lista_total = df_lista_total.drop_duplicates(ignore_index=True)

            if (len(df_lista_total) != 0 and len(df_lista_en_comun) != 0):
                df = pd.merge(df_lista_total, df_lista_en_comun, on=['cedula', 'cedula'], how="outer", indicator=True)
                df = df[df['_merge'] == 'left_only']
                df = df.reset_index()
                for index, row in df.iterrows():
                    if (not row.empty and row is not None):
                        persona = Persona()
                        persona.cedula = row.cedula
                        persona.cod_sexo = row.cod_sexo_x
                        persona.nombres = row.nombres_x
                        persona.fecha_nacimiento = row.fecha_nacimiento_x
                        persona.des_estado_civil = row.des_estado_civil_x
                        persona.cedula_padre = row.cedula_padre_x
                        persona.nombre_padre = row.nombre_padre_x
                        persona.cedula_madre = row.cedula_madre_x
                        persona.nombre_madre = row.nombre_madre_x
                        persona.cedula_conyuge = row.cedula_conyuge_x
                        persona.nombre_conyuge = row.nombre_conyuge_x
                        datos.append(persona)

                return datos
            else:
                return None
            
        elif (lista_total):

            for s in lista_total:
                if (s is not None):
                    lista_total_aux.append(s.to_dict())

            df_lista_total = pd.DataFrame.from_records(lista_total_aux)
            df_lista_total = df_lista_total.drop_duplicates(ignore_index=True)

            for index, row in df_lista_total.iterrows():
                if (not row.empty and row is not None):
                    persona = Persona()
                    persona.cedula = row.cedula
                    persona.cod_sexo = row.cod_sexo
                    persona.nombres = row.nombres
                    persona.fecha_nacimiento = row.fecha_nacimiento
                    persona.des_estado_civil = row.des_estado_civil
                    persona.cedula_padre = row.cedula_padre
                    persona.nombre_padre = row.nombre_padre
                    persona.cedula_madre = row.cedula_madre
                    persona.nombre_madre = row.nombre_madre
                    persona.cedula_conyuge = row.cedula_conyuge
                    persona.nombre_conyuge = row.nombre_conyuge
                    datos.append(persona)

            return datos
        else:
            return None

    @staticmethod
    def lista_sin_duplicados(datos, identificacion):
        datos_return = []
        for row_dato in range(len(datos)):
            if (int(datos[row_dato][0]) != int(identificacion)):
                dato = (datos[row_dato][0], datos[row_dato][1],
                        datos[row_dato][2], datos[row_dato][3],
                        datos[row_dato][4], datos[row_dato][5],
                        datos[row_dato][6], datos[row_dato][7],
                        datos[row_dato][8], datos[row_dato][9],
                        datos[row_dato][10])
                datos_return.append(dato)
                print(datos_return)

        return datos_return

    @staticmethod
    def lista_sin_duplicados_objeto(datos, identificacion):
        datos_return = []
        for row_dato in range(len(datos)):
            if (int(datos[row_dato].cedula) != int(identificacion)):
                dato = (datos[row_dato].cedula, int(datos[row_dato].cod_sexo), datos[row_dato].nombres,
                        datos[row_dato].fecha_nacimiento, datos[row_dato].des_estado_civil,
                        datos[row_dato].cedula_padre, datos[row_dato].nombre_padre,
                        datos[row_dato].cedula_madre, datos[row_dato].nombre_madre,
                        datos[row_dato].cedula_conyuge, datos[row_dato].nombre_conyuge)
                datos_return.append(dato)
        return datos_return

    @staticmethod
    def consulta_ascendencia(datos):
        herencia = []
        for row in datos:            
            if (row is not None and row.cedula_madre and row.cedula_madre != 0 and isinstance(row.cedula_madre, int)):
                informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == row.cedula_madre).first()
                herencia.append(informacion_consultada)
            elif (row is not None and row.nombre_madre is not None and len(row.nombre_madre.split(" ")) >= 3):
                informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombres.ilike(f"%{Utilidades.limpiar_espacios(row.nombre_madre)}%")).first()
                herencia.append(informacion_consultada)

            if (row is not None and row.cedula_padre and row.cedula_padre != 0):
                informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == row.cedula_padre).first()
                herencia.append(informacion_consultada)
            elif (row is not None and row.nombre_padre is not None and len(row.nombre_padre.split(" ")) >= 3 and isinstance(row.cedula_padre, int)):
                informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombres.ilike(f"%{Utilidades.limpiar_espacios(row.nombre_padre)}%")).first()
                herencia.append(informacion_consultada)

        return herencia


    @staticmethod
    def consulta_x_padres(datos):
        desendencia = []
        if (datos):
            for dato in datos:
                if (dato is not None and dato.nombre_padre and dato.nombre_madre):
                    informacion_consultada = session.query(TeDigercicCedulado).\
                    filter(TeDigercicCedulado.nombre_padre.ilike(f"{Utilidades.limpiar_espacios(dato.nombre_padre)}")).\
                    filter(TeDigercicCedulado.nombre_madre.ilike(f"{Utilidades.limpiar_espacios(dato.nombre_madre)}"))
                    for row in informacion_consultada:
                        desendencia.append(row)

        return desendencia

    @staticmethod
    def consulta_desendencia(datos):
        desendencia = []
        if (datos):
            for dato in datos:
                if (dato is not None and dato.cod_sexo == 1):
                    if (dato.cedula is not None and dato.cedula != 0):
                        informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula_padre == dato.cedula)
                        for row in informacion_consultada:
                            desendencia.append(row)
                    elif (dato.nombres is not None):
                        if (len(dato.nombres.split(" ")) >= 4):
                            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombre_padre.ilike(f"%{Utilidades.limpiar_espacios(dato.nombres)}%"))
                            for row in informacion_consultada:
                                desendencia.append(row)
                elif (dato is not None and dato.cod_sexo == 2):
                    if (dato.cedula is not None and dato.cedula != 0):
                        informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula_madre == dato.cedula)
                        for row in informacion_consultada:
                            desendencia.append(row)
                    elif (dato.nombres is not None):
                        if (len(dato.nombres.split(" ")) >= 4):
                            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombre_madre.ilike(f"%{Utilidades.limpiar_espacios(dato.nombres)}%"))
                            for row in informacion_consultada:
                                desendencia.append(row)

        return desendencia

    @staticmethod
    def consultar_info_usuario(cedula, nombres):
        usuario = []
        if (cedula is not None and cedula != 0):
            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == cedula).first()
            usuario.append(informacion_consultada)
        elif (nombres is not None and len(nombres.split(" ")) >= 4):
            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombres.ilike(f"%{Utilidades.limpiar_espacios(nombres)}%")).first()
            usuario.append(informacion_consultada)

        return usuario

    @staticmethod
    def consultar_info_x_usuario(cedula, nombres):
        informacion_consultada = None
        if (cedula and cedula != 0):
            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == cedula).first()
        elif (nombres is not None and len(nombres.split(" ")) >= 4):
            informacion_consultada = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.nombres.ilike(f"%{Utilidades.limpiar_espacios(nombres)}%")).first()

        return informacion_consultada
