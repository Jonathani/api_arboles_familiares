from flask import jsonify, request
from services.auth_guard import auth_guard
from util.errores import error_dato_null
from services.consulta_arbol_sql import consultar_arbol_familiar_sql
from services.consulta_arbol_nosql import consultar_arbol_familiar_nosql
from flask_cors import cross_origin

def init(app):

    @app.route('/api/consulta/arbolesFamiliaresSQL', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def consulta_arboles_familiares_sql():
        cedula_consultar = request.json["cedulasConsultar"]
        consulta_total = []
        if (cedula_consultar):
            consulta_total = consultar_arbol_familiar_sql(cedula_consultar)
            
            dato_consulta = {"consulta": consulta_total}
            return jsonify(dato_consulta), 200
        else:
            return jsonify(error_dato_null), 500
        
    @app.route('/api/consulta/arbolesFamiliaresNOSQL', methods=['POST'])
    @auth_guard()
    @cross_origin(supports_credentials=True)
    def consulta_arboles_familiares_nosql():
        cedula_consultar = request.json["cedulasConsultar"]
        consulta_total = []
        if (cedula_consultar):
            consulta_total = consultar_arbol_familiar_nosql(cedula_consultar)
            
            dato_consulta = {"consulta": consulta_total}
            return jsonify(dato_consulta), 200
        else:
            return jsonify(error_dato_null), 500