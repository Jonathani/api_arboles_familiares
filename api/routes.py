from flask import jsonify
from services.auth_guard import auth_guard

def init(app):

    @app.route('/api/protected_route', methods=['GET'])
    @auth_guard() # <--- Requires the authentication, but do not restricts authorization by roles
    def protected_route():
        return jsonify({"message": 'You have accessed a protected route.', "status": 200}), 200
