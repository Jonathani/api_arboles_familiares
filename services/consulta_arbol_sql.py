
import pandas as pd
from util.utilidades import Utilidades
import time
from entities.entitySQL.Cedulas import Cedulas, session
from entities.Persona import Persona

def consultar_arbol_familiar_sql(identificacion):

    informacion_total = []
    
    informacion_usuario = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == identificacion).first()
    informacion_usuario_lista = session.query(TeDigercicCedulado).filter(TeDigercicCedulado.cedula == identificacion)
    
    #Padres
    print(f"Consultando Padres de usuario {identificacion}, porfavor espere...")
    padres = Utilidades.consulta_ascendencia(informacion_usuario_lista) if informacion_usuario_lista else []

    if (padres):
        for padre in padres:
            if (padre and padre is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(padre, 'Consanguinidad', 'Padres', identificacion, informacion_usuario.nombres))
    else:
        for usuario in informacion_usuario_lista:
            if (usuario.cedula_padre == 0 and usuario.nombre_padre is not None):
                padre = Utilidades.armar_objeto(usuario.nombre_padre, 1)
                padres.append(padre)
            
            if (usuario.cedula_madre == 0 and usuario.nombre_madre is not None):
                padre = Utilidades.armar_objeto(usuario.nombre_madre, 2)
                padres.append(padre)

        if (padres):
            for padre in padres:
                if (padre is not None):
                    informacion_total.append(Utilidades.armar_informacion_objeto(padre, 'Consanguinidad', 'Padres', identificacion, informacion_usuario.nombres))

    #Hijos
    hijos = []
    print(f"Consultando Hijos de usuario {identificacion}, porfavor espere...")
    hijos = Utilidades.consulta_desendencia(informacion_usuario_lista)

    if (hijos):
        for hijo in hijos:
            if (hijo is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(hijo, 'Consanguinidad', 'Hijos', identificacion, informacion_usuario.nombres))

    #Hermanos
    hermanos = []
    hermanos_aux = []
    print(f"Consultando Hermanos de usuario {identificacion}, porfavor espere...")
    if (padres):
        hermanos_aux = Utilidades.consulta_desendencia(padres)

    if (hermanos_aux):
        for hermano in hermanos_aux:
            if (int(hermano.cedula) != int(identificacion)):
                hermanos.append(hermano)
                informacion_total.append(Utilidades.armar_informacion_objeto(hermano, 'Consanguinidad', 'Hermanos', identificacion, informacion_usuario.nombres))
    else:
        hermanos_aux = Utilidades.consulta_x_padres(informacion_usuario_lista)        
        if (hermanos_aux):
            for hermano in hermanos_aux:
                if (int(hermano.cedula) != int(identificacion)):
                    hermanos.append(hermano)
                    informacion_total.append(Utilidades.armar_informacion_objeto(hermano, 'Consanguinidad', 'Hermanos', identificacion, informacion_usuario.nombres))

    #Abuelos
    abuelos = []
    print(f"Consultando Abuelos de usuario {identificacion}, porfavor espere...")    
    if (padres):
        abuelos = Utilidades.consulta_ascendencia(padres)
        if (abuelos):
            for abuelo in abuelos:
                if (abuelo):
                    informacion_total.append(Utilidades.armar_informacion_objeto(abuelo, 'Consanguinidad', 'Abuelos', identificacion, informacion_usuario.nombres))
                else:
                    if (padres):
                        for padre in padres:
                            if (padre is not None):
                                if (padre.cedula_padre == 0 and padre.nombre_padre is not None and padre.nombre_padre != ""):
                                    abuelo = Utilidades.armar_objeto(padre.nombre_padre, 1)
                                    abuelos.append(abuelo)
                            
                                if (padre.cedula_madre == 0 and padre.nombre_madre is not None and padre.nombre_madre != ""):
                                    abuelo = Utilidades.armar_objeto(padre.nombre_madre, 2)
                                    abuelos.append(abuelo)
                                
                        if (abuelos):
                            for abuelo in abuelos:
                                if (abuelo is not None):
                                    informacion_total.append(Utilidades.armar_informacion_objeto(abuelo, 'Consanguinidad', 'Abuelos', identificacion, informacion_usuario.nombres))
        else:
            for padre in padres:
                if (padre is not None):
                    if (padre.cedula_padre == 0 and padre.nombre_padre is not None and padre.nombre_padre != ""):
                        abuelo = Utilidades.armar_objeto(padre.nombre_padre, 1)
                        abuelos.append(abuelo)
                
                    if (padre.cedula_madre == 0 and padre.nombre_madre is not None and padre.nombre_madre != ""):
                        abuelo = Utilidades.armar_objeto(padre.nombre_madre, 2)
                        abuelos.append(abuelo)
                    
            if (abuelos):
                for abuelo in abuelos:
                    if (abuelo is not None):
                        informacion_total.append(Utilidades.armar_informacion_objeto(abuelo, 'Consanguinidad', 'Abuelos', identificacion, informacion_usuario.nombres))
    #Nietos
    nietos = []
    print(f"Consultando Nietos de usuario {identificacion}, porfavor espere...")
    if (hijos):
        nietos = Utilidades.consulta_desendencia(hijos)

    for nieto in nietos:
        if (nieto is not None):
            informacion_total.append(Utilidades.armar_informacion_objeto(nieto, 'Consanguinidad', 'Nietos', identificacion, informacion_usuario.nombres))

    #Bisabuelos    
    bisabuelos = []
    print(f"Consultando Bisabuelos de usuario {identificacion}, porfavor espere...")
    if (abuelos):
        bisabuelos = Utilidades.consulta_ascendencia(abuelos)
  
    if (bisabuelos is not None):
        for bisabuelo in bisabuelos:
            if (bisabuelo is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(bisabuelo, 'Consanguinidad', 'Bisabuelos', identificacion, informacion_usuario.nombres))

    #Bisnietos
    bisnietos = []
    print(f"Consultando Bisnietos de usuario {identificacion}, porfavor espere...")
    if (nietos):
        bisnietos = Utilidades.consulta_desendencia(nietos)

    if (bisnietos):
        for bisnieto in bisnietos:
            if (bisnieto is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(bisnieto, 'Consanguinidad', 'Bisnietos', identificacion, informacion_usuario.nombres))

    #Tios
    tios = []
    tios_aux = []
    print(f"Consultando Tios de usuario {identificacion}, porfavor espere...")
    if (abuelos):
        tios_aux = Utilidades.consulta_desendencia(abuelos)
    
        if (tios_aux):
            tios_auxiliar = Utilidades.lista_sin_repetidos_objetos(padres, tios_aux)
            for tio in tios_auxiliar:
                if (tio is not None):
                    tios.append(tio)
                    informacion_total.append(Utilidades.armar_informacion_objeto(tio, 'Consanguinidad', 'Tios', identificacion, informacion_usuario.nombres))

   
    if (padres):
        tios_aux = Utilidades.consulta_x_padres(padres)

        if (tios_aux):
            tios_auxiliar = Utilidades.lista_sin_repetidos_objetos(padres, tios_aux)
            for tio in tios_auxiliar:
                if (tio is not None):                    
                    tios.append(tio)
                    informacion_total.append(Utilidades.armar_informacion_objeto(tio, 'Consanguinidad', 'Tios', identificacion, informacion_usuario.nombres))
    #Sobrinos
    sobrinos = []
    print(f"Consultando Sobrinos de usuario {identificacion}, porfavor espere...")
    if (hermanos):
        sobrinos = Utilidades.consulta_desendencia(hermanos)

    if (sobrinos):
        for sobrino in sobrinos:
            if (sobrino is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(sobrino, 'Consanguinidad', 'Sobrinos', identificacion, informacion_usuario.nombres))

    #Tatarabuelos
    tatarabuelos = []
    print(f"Consultando Tatarabuelos de usuario {identificacion}, porfavor espere...")
    if (bisabuelos is not None):
        tatarabuelos = Utilidades.consulta_ascendencia(bisabuelos)

    if (tatarabuelos is not None):
        for tatarabuelo in tatarabuelos:
            if (tatarabuelo and tatarabuelo is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(tatarabuelo, 'Consanguinidad', 'Tatarabuelos', identificacion, informacion_usuario.nombres))

    #Tataranietos
    tataranietos = []
    print(f"Consultando Tataranietos de usuario {identificacion}, porfavor espere...")
    if (bisnietos):
        tataranietos = Utilidades.consulta_desendencia(bisnietos)

    if (tataranietos):
        for bisnieto in tataranietos:
            if (bisnieto and bisnieto is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(tataranietos, 'Consanguinidad', 'Tataranietos', identificacion, informacion_usuario.nombres))
    
    #Primos
    primos = []
    print(f"Consultando Primos de usuario {identificacion}, porfavor espere...")
    if (tios):
        primos = Utilidades.consulta_desendencia(tios)

    if (primos):
        primos = Utilidades.lista_sin_repetidos_objetos(hermanos, primos)
        for primo in primos:
            if (primo is not None and identificacion != primo.cedula):
                informacion_total.append(Utilidades.armar_informacion_objeto(primo, 'Consanguinidad', 'Primos', identificacion, informacion_usuario.nombres))

    #TiosAbuelos
    print(f"Consultando Tios Abuelos de usuario {identificacion}, porfavor espere...")
    tios_abuelos = []
    if (bisabuelos):
        tios_abuelos = Utilidades.consulta_desendencia(bisabuelos)

    if (tios_abuelos):
        for tio_abuelo in tios_abuelos:
            if (tio_abuelo is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(tio_abuelo, 'Consanguinidad', 'TiosAbuelos', identificacion, informacion_usuario.nombres))
    
    #SobrinosNietos
    sobrinos_nietos = []
    print(f"Consultando Sobrinos Nietos de usuario {identificacion}, porfavor espere...")
    if (sobrinos):
        sobrinos_nietos = Utilidades.consulta_desendencia(sobrinos)

    if (sobrinos_nietos):
        for sobrino_nieto in sobrinos_nietos:
            if (sobrino_nieto is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(sobrino_nieto, 'Consanguinidad', 'SobrinosNietos', identificacion, informacion_usuario.nombres))

    ############################################################# Consulta por Afinidad ################################################################

    #Conyugue
    print(f"Consultando Conyuge de usuario {identificacion}, porfavor espere...")
    conyugues = Utilidades.consultar_info_usuario(informacion_usuario.cedula_conyuge , informacion_usuario.nombre_conyuge)

    if (conyugues):
        for conyugue in conyugues:
            if (conyugue is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(conyugue, 'Afinidad', 'Conyugues', identificacion, informacion_usuario.nombres))

    #Suegros
    suegros = []
    print(f"Consultando Suegros de usuario {identificacion}, porfavor espere...")
    if (conyugues):
        suegros = Utilidades.consulta_ascendencia(conyugues)

    if (suegros):
        for suegro in suegros:
            if (suegro is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(suegro, 'Afinidad', 'Suegros', identificacion, informacion_usuario.nombres))
    else:
        if (conyugues):
            for conyugue in conyugues:
                if (conyugue and conyugue.cedula_padre == 0 and conyugue.nombre_padre is not None):
                    suegro = Utilidades.armar_objeto(conyugue.nombre_padre, 1)
                    padres.append(suegro)
                
                if (conyugue and conyugue.cedula_madre == 0 and conyugue.nombre_madre is not None):
                    suegro = Utilidades.armar_objeto(conyugue.nombre_madre, 2)
                    suegros.append(suegro)

            if (suegros):
                for suegro in suegros:
                    if (suegro):
                        informacion_total.append(Utilidades.armar_informacion_objeto(suegro, 'Afinidad', 'Suegros', identificacion, informacion_usuario.nombres))

    #Yernos y yernas
    print(f"Consultando Yernos de usuario {identificacion}, porfavor espere...")
    yernos = []
    if (hijos):
        for hijo in hijos:
            yerno = Utilidades.consultar_info_x_usuario(hijo.cedula_conyuge , hijo.nombre_conyuge)
            if (yerno is not None):
                yernos.append(yerno)

    if (yernos):
        for yerno in yernos:
            if (yerno):
                informacion_total.append(Utilidades.armar_informacion_objeto(yerno, 'Afinidad', 'Yernos', identificacion, informacion_usuario.nombres))
    
    #Padrastro y madrastra
    padrastros = []
    padrastros_aux = []
    print(f"Consultando Padrastro de usuario {identificacion}, porfavor espere...")
    if (padres):
        for padre in padres:
            if (padre is not None and padre.cedula_conyuge is not None and padre.cedula_conyuge != 0 and padre.nombre_conyuge):
                padrastro_aux = Utilidades.consultar_info_x_usuario(padre.cedula_conyuge, padre.nombre_conyuge)
                if (padrastro_aux is not None):
                    padrastros_aux.append(padrastro_aux)

    if (padrastros_aux):
        padrastros = Utilidades.lista_sin_repetidos_objetos(padres, padrastros_aux)
        for padrastro in padrastros:
            if (padrastro is not None):                
                informacion_total.append(Utilidades.armar_informacion_objeto(padrastro, 'Afinidad', 'Padrastros', identificacion, informacion_usuario.nombres))


    #Hijastros
    print(f"Consultando Hijastro de usuario {identificacion}, porfavor espere...")
    hijastros = []
    hijastros_aux = []
    if (conyugues):
        hijastros_aux = Utilidades.consulta_desendencia(conyugues)

    if (hijastros_aux):
        hijastros = Utilidades.lista_sin_repetidos_objetos(hijos, hijastros_aux)
        for hijastro in hijastros:
            if (hijastro is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(hijastro, 'Afinidad', 'Hijastros', identificacion, informacion_usuario.nombres))


    #Abuelos conyuge
    abuelos_conyugues = []
    if (suegros):
        abuelos_conyugues = Utilidades.consulta_ascendencia(suegros)

    if (abuelos_conyugues):
        for abuelo_conyugue in abuelos_conyugues:
            if (abuelo_conyugue  is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(abuelo_conyugue, 'Afinidad', 'AbuelosConyugue', identificacion, informacion_usuario.nombres))

    #Cónyuges de los Nietos
    conyuges_nietos = []
    conyuge_nieto = []
    print(f"Consultando Cónyuges de los Nietos de usuario {identificacion}, porfavor espere...")
    if(nietos):
        for nieto in nietos:
            conyuge_nieto = Utilidades.consultar_info_x_usuario(nieto.cedula_conyuge , nieto.nombre_conyuge)
            if (conyuge_nieto is not None):
                conyuges_nietos.append(conyuge_nieto)

    if (conyuges_nietos):
        for conyuge_nieto in conyuges_nietos:
            if (conyuge_nieto is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(conyuge_nieto, 'Afinidad', 'ConyuguesNietos', identificacion, informacion_usuario.nombres))


    #Cuñados
    cuniados = []
    cuniados_aux = []
    print(f"Consultando Cuñados de usuario {identificacion}, porfavor espere...")
    if (suegros):
        cuniados_aux = Utilidades.consulta_desendencia(suegros)

    if (cuniados_aux):
        for cuniado in cuniados_aux:
            if (cuniado is not None and cuniado.cedula != informacion_usuario.cedula_conyuge and cuniado.nombres != informacion_usuario.nombre_conyuge):
                cuniados.append(cuniado)
                informacion_total.append(Utilidades.armar_informacion_objeto(cuniado, 'Afinidad', 'Cuniados', identificacion, informacion_usuario.nombres))
    else:
        cuniados_aux = Utilidades.consulta_x_padres(conyugues)
        for cuniado in cuniados_aux:
            if (cuniado is not None and cuniado.cedula != informacion_usuario.cedula_conyuge and cuniado.nombres != informacion_usuario.nombre_conyuge):
                cuniados.append(cuniado)
                informacion_total.append(Utilidades.armar_informacion_objeto(cuniado, 'Afinidad', 'Cuniados', identificacion, informacion_usuario.nombres))

    
    #Hermanastros
    hermanastros = []
    hermanastros_aux = []
    print(f"Consultando Hermanastros de usuario {identificacion}, porfavor espere...")
    if (padrastros):
        hermanastros_aux = Utilidades.consulta_desendencia(padrastros)

    if (hermanastros_aux is not None):
        hermanastros = Utilidades.lista_sin_repetidos_objetos(hermanos, hermanastros_aux)
        if (hermanastros):
            for hermanastro in hermanastros:
                if (hermanastro is not None):
                    informacion_total.append(Utilidades.armar_informacion_objeto(hermanastro, 'Afinidad', 'Hermanastros', identificacion, informacion_usuario.nombres))

    #Abuelastros
    abuelastros = []
    print(f"Consultando Abuelastros de usuario {identificacion}, porfavor espere...")
    if (padrastros):
        abuelastros = Utilidades.consulta_ascendencia(padrastros)

    if (abuelastros):
        for abuelastro in abuelastros:
            if (abuelastro is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(abuelastro, 'Afinidad', 'Abuelastros', identificacion, informacion_usuario.nombres))

    #Nietrastos
    print(f"Consultando Nietrastos de usuario {identificacion}, porfavor espere...")
    nietrastos = []
    if (hijastros):
        nietrastos = Utilidades.consulta_desendencia(hijastros)

    if (nietrastos):
        for nietrasto in nietrastos:
            if (nietrasto is not None):
                informacion_total.append(Utilidades.armar_informacion_objeto(nietrasto, 'Afinidad', 'Nietrastos', identificacion, informacion_usuario.nombres))


    df = pd.DataFrame(informacion_total)
    df = df.drop_duplicates(ignore_index = True)
    
    informacion_total = df.to_dict('records')
    return informacion_total


