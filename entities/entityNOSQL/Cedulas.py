from entities.conexion import Document, StringField, IntField, DateField, archivo_configuracion

class Cedulas(Document):
    cedula = IntField(required=True)
    cod_sexo = IntField()
    nombres =StringField()
    fecha_nacimiento = DateField()
    des_estado_civil =StringField()
    cedula_padre = IntField()
    nombre_padre = StringField()
    cedula_madre = IntField()
    nombre_madre = StringField()
    cedula_conyuge = IntField()
    nombre_conyuge = StringField()

    def to_dict(self):
        return {
            'cedula': self.cedula,
            'cod_sexo': self.cod_sexo,
            'nombres': self.nombres,
            'fecha_nacimiento': self.fecha_nacimiento,
            'des_estado_civil': self.des_estado_civil,
            'cedula_padre': self.cedula_padre,
            'nombre_padre': self.nombre_padre,
            'cedula_madre': self.cedula_madre,
            'nombre_madre': self.nombre_madre,
            'cedula_conyuge': self.cedula_conyuge,
            'nombre_conyuge': self.nombre_conyuge,
        }

    meta = {'db_alias': archivo_configuracion['mongodb']['bdd_alias_mongodb'], 'collection': archivo_configuracion['mongodb']['collection_mongodb']}
