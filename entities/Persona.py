
class Persona:
    cedula = 0
    cod_sexo = 0
    nombres = ""
    fecha_nacimiento = ""
    des_estado_civil = ""
    cedula_padre = 0
    nombre_padre = ""
    cedula_madre = 0
    nombre_madre = ""
    cedula_conyuge = 0
    nombre_conyuge = ""

    def to_dict(self):
        return {
            'cedula': self.cedula,
            'cod_sexo': self.cod_sexo,
            'nombres': self.nombres,
            'fecha_nacimiento': self.fecha_nacimiento,
            'des_estado_civil': self.des_estado_civil,
            'cedula_padre': self.cedula_padre,
            'nombre_padre': self.nombre_padre,
            'cedula_madre': self.cedula_madre,
            'nombre_madre': self.nombre_madre,
            'cedula_conyuge': self.cedula_conyuge,
            'nombre_conyuge': self.nombre_conyuge,
        }

