from sqlalchemy.ext.automap import automap_base
from sqlalchemy import Column, String, Integer, Date
from sqlalchemy.orm import Session
from entities.conexion import engine

Base = automap_base()

class TeDigercicCedulado(Base):
    __tablename__ = "cedulas"
    __table_args__ = {"schema": "extraccion"}

    cedula = Column('cedula', Integer, primary_key=True)
    cod_sexo = Column('cod_sexo', Integer)
    nombres = Column('nombres', String)
    fecha_nacimiento = Column('fecha_nacimiento', Date)
    des_estado_civil = Column('des_estado_civil', String)
    cedula_padre = Column('cedula_padre', Integer)
    nombre_padre = Column('nombre_padre', String)
    cedula_madre = Column('cedula_madre', Integer)
    nombre_madre = Column('nombre_madre', String)
    cedula_conyuge = Column('cedula_conyuge', Integer)
    nombre_conyuge = Column('nombre_conyuge', String)

    def __init__(self, cedula, cod_sexo, nombres, fecha_nacimiento, des_estado_civil, cedula_padre, nombre_padre, cedula_madre, nombre_madre, cedula_conyuge, nombre_conyuge):
        self.cedula = cedula
        self.cod_sexo = cod_sexo
        self.nombres = nombres
        self.fecha_nacimiento = fecha_nacimiento
        self.des_estado_civil = des_estado_civil
        self.cedula_padre = cedula_padre
        self.cedula_madre = cedula_madre
        self.nombre_padre = nombre_padre
        self.nombre_madre = nombre_madre
        self.cedula_conyuge = cedula_conyuge
        self.nombre_conyuge = nombre_conyuge

    def to_dict(self):
        return {
            'cedula': self.cedula,
            'cod_sexo': self.cod_sexo,
            'nombres': self.nombres,
            'fecha_nacimiento': self.fecha_nacimiento,
            'des_estado_civil': self.des_estado_civil,
            'cedula_padre': self.cedula_padre,
            'nombre_padre': self.nombre_padre,
            'cedula_madre': self.cedula_madre,
            'nombre_madre': self.nombre_madre,
            'cedula_conyuge': self.cedula_conyuge,
            'nombre_conyuge': self.nombre_conyuge,
        }


Base.prepare(autoload_with=engine)
session = Session(engine)
