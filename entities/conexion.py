import psycopg2
import tomli
from sqlalchemy import create_engine
from mongoengine import Document, StringField, IntField, DateField, connect

archivo_configuracion = tomli.load(open(r"./configuracion.toml", mode="rb"))

#Conexion a las bdd SQL -> Postgres
db_string = f"postgresql://{archivo_configuracion['dbpostgres']['user_dbpostgres']}:{archivo_configuracion['dbpostgres']['pass_dbpostgres']}@{archivo_configuracion['dbpostgres']['host_dbpostgres']}:{archivo_configuracion['dbpostgres']['puerto_dbpostgres']}/{archivo_configuracion['dbpostgres']['bdd_dbpostgres']}"
db = create_engine(db_string)

url_string = f"postgresql://{archivo_configuracion['dbpostgres']['user_dbpostgres']}:{archivo_configuracion['dbpostgres']['pass_dbpostgres']}@{archivo_configuracion['dbpostgres']['host_dbpostgres']}:{archivo_configuracion['dbpostgres']['puerto_dbpostgres']}/{archivo_configuracion['dbpostgres']['bdd_dbpostgres']}"
engine = create_engine(url_string)

conexion_stage = psycopg2.connect(host=archivo_configuracion['dbpostgres']['host_dbpostgres'], port = archivo_configuracion['dbpostgres']['puerto_dbpostgres'], database=archivo_configuracion['dbpostgres']['bdd_dbpostgres'], user=archivo_configuracion['dbpostgres']['user_dbpostgres'], password=archivo_configuracion['dbpostgres']['pass_dbpostgres'])
cursor_stage = conexion_stage.cursor()

#Conexion a las bdd NOSQL -> MongoDB
connect(
    alias = archivo_configuracion['mongodb']['bdd_alias_mongodb'],
    db = archivo_configuracion['mongodb']['bdd_mongodb'],
    host=archivo_configuracion['mongodb']['host_mongodb'],
    port= archivo_configuracion['mongodb']['puerto_mongodb'],
    username= archivo_configuracion['mongodb']['user_mongodb'],
    password= archivo_configuracion['mongodb']['pass_mongodb'],    
)